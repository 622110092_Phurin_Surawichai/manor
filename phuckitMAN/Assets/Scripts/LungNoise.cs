﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungNoise : MonoBehaviour
{
    [SerializeField] float speed = 3.5f;

    public AnimationState currentAnimation;
    public Animator animator;
    public AudioSource _sfx;
    public AudioSource _died;
    private bool isMoving = false;
    private Rigidbody2D body;

    public GameObject Gameover;

    public GameObject EseMenu;


    private bool Take_item;
    public VirtualBottonState _takeitem;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        body = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        float inputX = Input.GetAxisRaw("Horizontal");
        float inputY = Input.GetAxisRaw("Vertical");

        //swap facing,move
        if (inputX > 0)
        {
            //Turn right

            //GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<Transform>().eulerAngles = new Vector3(0, 0, -90);
            transform.Translate(inputX * speed * Time.deltaTime, inputY * speed * Time.deltaTime, 0f, Space.World);
            isMoving = true;
        }
        else if (inputX < 0)
        {
            //Turn left

            //GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 90);
            transform.Translate(inputX * speed * Time.deltaTime, inputY * speed * Time.deltaTime, 0f, Space.World);
            isMoving = true;
        }
        else if (inputY > 0)
        {
            //Turn up

            GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 0);
            transform.Translate(inputX * speed * Time.deltaTime, inputY * speed * Time.deltaTime, 0f, Space.World);
            isMoving = true;
        }
        else if (inputY < 0)
        {
            //Turn down

            GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 180);
            transform.Translate(inputX * speed * Time.deltaTime, inputY * speed * Time.deltaTime, 0f, Space.World);
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
        //esc menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EseMenu.SetActive(true);
        }


        /////Mobile

        //animate movement
        animator.SetFloat("speed", Mathf.Abs(inputX) + Mathf.Abs(inputY) - 100);
        //SFX
        if (isMoving)
        {
            if (isMoving == true)
            {
                _sfx.Play();
            }
            else
            {
                _sfx.Stop();
            }
        }
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("Enemy"))
        {
            {
                _died.Play();
                Gameover.SetActive(true);
                Destroy(this);
            }
        }
    }
}