﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    public GameObject currentInteractObj = null;
    public InteractObj currentInteractObjScript = null;
    public Inventory inventory;

    private void Update()
    {
        if(Input.GetButtonDown("InteractWithObj")&&currentInteractObj)
        {
            if(currentInteractObjScript.inventory)
            {
                //called from inventory
                inventory.addItem(currentInteractObj);
            }
            else if(currentInteractObjScript.openable)
            {
                if (currentInteractObjScript.locked)
                {
                    //check the key item
                    if(inventory.FindItem(currentInteractObjScript.itemNeeded))
                    {
                        currentInteractObjScript.locked = false;
                        Debug.Log(currentInteractObj.name + " unlocked");
                    }
                    else
                    {
                        Debug.Log(currentInteractObj.name + " still locked");
                    }
                }
                else
                {
                    //object not locked anymore -> open the object
                    Debug.Log(currentInteractObj.name + " opened");
                    currentInteractObj.SetActive(false);
                }
            } 
            //for books
            else if(currentInteractObjScript.readable)
            {
                if (currentInteractObjScript.outObj == false)
                {
                    currentInteractObjScript.Read();
                }
                else
                {
                    currentInteractObjScript.CloseRead();
                }
            }
            //for candles
            else if(currentInteractObjScript.extinguishable)
            {
                Debug.Log(currentInteractObj.name + " extinguish");
                currentInteractObj.SetActive(false);
                
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Interactable"))
        {
            Debug.Log(other.name);
            currentInteractObj = other.gameObject;
            currentInteractObjScript = currentInteractObj.GetComponent<InteractObj>();
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Interactable"))
        {
            if(other.gameObject == currentInteractObj)
            {
                currentInteractObj = null;
            }
            
        }
    }
}
