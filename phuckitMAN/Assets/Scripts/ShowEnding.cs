﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowEnding : MonoBehaviour
{
    public GameObject endingscene;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("player"))
        {
            endingscene.SetActive(true);
            
        }
    }
}
