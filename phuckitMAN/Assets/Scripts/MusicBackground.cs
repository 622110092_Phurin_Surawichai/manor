﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBackground : MonoBehaviour
{
    public AudioSource _music;
    public static MusicBackground singleton;
    // Start is called before the first frame update
    void Start()
    {
        if (singleton)
        {
            DestroyImmediate(gameObject);
            return;
        }
    }
    //singleton = this;
    //DontDestroyOnLoad(gameObject);
}
