﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialog : MonoBehaviour
{
    public string[] dialog;
    public Text _currentText;
    public int indexText = 0;
    
    public Image _currentImage;
    public int indexImage = 0;
    
    private void OnEnable()
    {
        _currentText.text = dialog[indexText];
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if(indexText<dialog.Length-1)
            {
                indexText++;
                _currentText.text = dialog[indexText];
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}
