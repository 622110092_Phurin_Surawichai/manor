﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomActive : MonoBehaviour
{
    public GameObject _currentRoom;
    public GameObject _nextRoom;
    public GameObject _LungNoise;
    public Transform _spawnPoint;

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("player"))
        {
            _nextRoom.SetActive(true);
            _currentRoom.SetActive(false);
            
        }
    }
}
