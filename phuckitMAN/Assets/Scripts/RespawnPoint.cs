﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Cinemachine;

public class RespawnPoint : MonoBehaviour
{
    public static RespawnPoint singleton;

    public int countPlayer=1;

    public Transform respawnPoint;
    public GameObject playerPrefab;

    //public CinemachineVirtualCameraBase cam;

    void Start()
    {
        singleton = this;
    }
    private void Update()
    {
        if(countPlayer<1)
        {
            Respawn();
        }
    }
    public void Respawn()
    {
        GameObject player = Instantiate(playerPrefab/*, respawnPoint.position, Quaternion.identity*/);
        player.transform.position = respawnPoint.position;
        player.GetComponentInChildren<LungNoise>();
        //cam.Follow = player.transform;
    }
}
