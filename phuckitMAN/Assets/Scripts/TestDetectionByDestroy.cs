﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDetectionByDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerStay2D(Collider2D collision)
    {
            if (collision.tag.Contains("Enemy"))
            {
                Destroy(gameObject);
            }
    }
}
