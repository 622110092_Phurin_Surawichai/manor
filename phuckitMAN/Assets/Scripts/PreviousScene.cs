﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PreviousScene : MonoBehaviour
{
    private int loadPreviousScene;
    void Start()
    {
        loadPreviousScene = SceneManager.GetActiveScene().buildIndex - 1;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("player"))
        {
            SceneManager.LoadScene(loadPreviousScene);
        }
        
    }
}
