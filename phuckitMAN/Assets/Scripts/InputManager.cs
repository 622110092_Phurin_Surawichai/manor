﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public string xAxis = "Horizontal";
    public static InputManager singleton;
    // Start is called before the first frame update
    void Start()
    {
        singleton = this;
    }

    // Update is called once per frame
    public float getXAxis()
    {
        return Input.GetAxisRaw(xAxis);
    }
}
