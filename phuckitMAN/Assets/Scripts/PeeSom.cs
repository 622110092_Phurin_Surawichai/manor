﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeeSom : MonoBehaviour
{
    [SerializeField] public float speed = 0.5f;

    public Transform LungNoise;
    private Rigidbody2D direc;
    private Vector2 movement;
    // Start is called before the first frame update
    void Start()
    {
        direc = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //facing & get player position
        Vector3 direction = LungNoise.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90.0f;
        direc.rotation = angle;
        direction.Normalize();
        movement = direction;
        //Debug.Log(direction);
    }
    private void FixedUpdate()
    {
        PeeSomMove(movement);
    }
    void PeeSomMove(Vector2 direction)
    {
        direc.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
    }
}
