﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIManagement : MonoBehaviour 
{ 
    public GameObject m_StartPanel;
    public GameObject m_OptionPanel;
    private int loadNextScene;

    void Start()
    {
        loadNextScene = SceneManager.GetActiveScene().buildIndex + 1;
    }
    public void OnStartEvent() 
    {
        m_StartPanel.SetActive(true);
        m_OptionPanel.SetActive(false);
    } 
    public void OnOptionEvent()
    { 
        m_StartPanel.SetActive(false);
        m_OptionPanel.SetActive(true);
    } 
    public void OnExitEvent() 
    { 
        Application.Quit();
    } 
    public void OnSlideValueUpdateEvent(Slider _slider)
    {
        Debug.Log("Value : " + _slider.value);
        MusicBackground.singleton._music.volume = _slider.value;
    } 
    public void OnToggleUpdateEvent(Toggle _tog) 
    { 
        Debug.Log("Value : " + _tog.isOn);
        MusicBackground.singleton._music.mute = !_tog.isOn;
    }
    public void OnLoadSceneEvent()
    {
        SceneManager.LoadScene(loadNextScene);
    }
}
