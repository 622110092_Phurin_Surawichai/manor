﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject[] inventory=new GameObject[15];
    public Button[] inventoryButton = new Button[15];
    public static Inventory singleton;
    public void addItem(GameObject item)
    {
        bool itemAdded = false;
        for (int i=0;i<inventory.Length;i++)
        {
            if (inventory[i]==null)
            {
                inventory[i] = item;
                inventoryButton[i].image.overrideSprite = item.GetComponent<SpriteRenderer>().sprite;
                Debug.Log(item.name + " added");
                itemAdded = true;
                item.SendMessage("DoInteraction");
                break;
            }
        }
        if(!itemAdded)
        {
            Debug.Log("Inventory full - Item not added");
        }
    }
    public bool FindItem(GameObject item)
    {
        for (int i = 0;i<inventory.Length;i++)
        {
            if (inventory [i]==item)
            {
                return true;
            }
        }
        return false;
    }
}
