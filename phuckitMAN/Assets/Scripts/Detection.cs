﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour
{
    public bool playerDetected = false;
    private Rigidbody2D currSpeed;
    private void Start()
    {
        currSpeed = this.GetComponent<Rigidbody2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Contains("Wall"))
        {
            playerDetected = false;
            currSpeed.bodyType = RigidbodyType2D.Static;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag.Contains("player")&& playerDetected == false)
        {
            playerDetected = true;
            currSpeed.bodyType = RigidbodyType2D.Dynamic;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        playerDetected = false;
        currSpeed.bodyType = RigidbodyType2D.Static;
    }
}
