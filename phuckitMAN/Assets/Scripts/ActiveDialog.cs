﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveDialog : MonoBehaviour
{
    public GameObject _ActiveDialog;

    private bool m_take;
    public VirtualBottonState _takemobile;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && _ActiveDialog.activeSelf == false)
        {
            _ActiveDialog.SetActive(true);
        }
        if (_takemobile._currenState == VirtualBottonState.State.Down && m_take == false)
        {
            _ActiveDialog.SetActive(true);
        }
    }
}
