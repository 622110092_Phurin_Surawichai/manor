﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GotoHint : MonoBehaviour
{
    public void GoHint()
    {
        SceneManager.LoadScene("Hint");
    }
}