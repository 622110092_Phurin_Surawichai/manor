﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool GameIsPause = false;

    public GameObject pauseMenuUI;
    void Start()
    {
       
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPause)
            {
                Paused();
            }
            else
            {
                Resume();
            }
        }
    }
    // Update is called once per frame
    void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 0f;
        GameIsPause = false;
    }

    private void Paused()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPause = true;
    }

    public void LoadMenu()
    {

    }
}