﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualBottonState : MonoBehaviour
{
    public enum State { Up, Down }
    public State _currenState;
    public void SetDownState()
    {
        _currenState = State.Down;
        print("Button : " + this.name + " is " + _currenState.ToString());
    }

    // Update is called once per frame
    public void SetupSate()
    {
        _currenState = State.Up;
        print("Button : " + this.name + " is " + _currenState.ToString());
    }
}

