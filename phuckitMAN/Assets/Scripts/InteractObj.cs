﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractObj : MonoBehaviour
{
    public bool inventory;
    public bool locked;
    public bool openable;
    public bool readable;
    public bool extinguishable;

    public Text bookInfo;

    public string message;

    public GameObject itemNeeded;
    public GameObject textBox;
    public GameObject LastDoor;

    public bool outObj = false;

    public void DoInteraction()
    {
        gameObject.SetActive(false);
    }
    public void Read()
    {
        Debug.Log(message);
        bookInfo.text=(message);
        textBox.SetActive(true);
        outObj = true;
        Debug.Log(textBox.name + (" is on"));
        Debug.Log(" " + outObj);
    }
    public void CloseRead()
    {
        textBox.SetActive(false);
        outObj = false;
        Debug.Log(textBox.name + (" is off"));
        Debug.Log(" " + outObj);
    }
}
